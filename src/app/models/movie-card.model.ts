import { environment } from '../../environments/environment';
import { MovieListResultObject, isMovieListResultObject, PosterSizes } from 'app/utils/tmdb-api.types';

export class MovieCard {
    id: number;
    title: string;
    posterUrl: string | null;

    public static getImageUrl(obj: MovieListResultObject | string,
                              size: PosterSizes): string | null {
        if (!obj) { return null; }
        if (isMovieListResultObject(obj)) {
            return environment.theMovieDB.imagesUrl + size + '/' + obj.poster_path;
        }
        return environment.theMovieDB.imagesUrl + size + '/' + obj;
    }

    constructor(movieCardInfo: MovieListResultObject) {
        if (!isMovieListResultObject(movieCardInfo)) { return null; }
        this.id = movieCardInfo.id;
        this.title = movieCardInfo.title;
        this.posterUrl = MovieCard.getImageUrl(movieCardInfo, PosterSizes.WIDTH_185);
    }
}
