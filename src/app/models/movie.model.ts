import { MovieDetailsWithCredits, PosterSizes } from '../utils/tmdb-api.types';
import { MovieCard } from './movie-card.model';

export class MovieDetails {
    id: number;
    title: string;
    overview: string;
    genres: string[];
    posterUrl: string | null;
    backdropUrl: string;
    releaseDate: Date;
    director: string;
    actors: {
        character: string;
        name: string;
    }[];
    popularity: number;
    voteCount: number;
    voteAverage: number;

    constructor(obj: MovieDetailsWithCredits) {
        if (!obj) { return null; }
        this.id = obj.id;
        this.title = obj.title;
        this.overview = obj.overview;
        this.genres = obj.genres.map((genre) => genre.name);
        this.posterUrl = MovieCard.getImageUrl(obj.poster_path, PosterSizes.WIDTH_342);
        this.backdropUrl = MovieCard.getImageUrl(obj.backdrop_path, PosterSizes.WIDTH_500);
        this.releaseDate = new Date(obj.release_date);
        this.director = obj.credits.crew.find((person) => person.job === 'Director').name;
        this.actors = obj.credits.cast.map((person) => {
            return {name: person.name, character: person.character}
        });
        this.popularity = obj.popularity;
        this.voteCount = obj.vote_count;
        this.voteAverage = obj.vote_average;
    }

    public getActorsNames(limit?: number): string[] {
        if (!limit) {
            return this.actors.map((a) => a.name);
        }
        return this.actors.slice(0, limit).map((a) => a.name);
    }
}
