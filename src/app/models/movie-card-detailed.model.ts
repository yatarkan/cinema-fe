import { MovieCard } from './movie-card.model';
import { MovieListResultObject } from '../utils/tmdb-api.types';

export class MovieCardDetailed extends MovieCard {
    rating: number;
    releaseDate: Date;
    genres: string[];

    constructor(obj: MovieListResultObject, stringGenres: string[]) {
        if (!obj || !stringGenres) { return null; }
        super(obj);
        this.rating = obj.vote_average;
        this.releaseDate = new Date(obj.release_date);
        this.genres = stringGenres;
    }

}
