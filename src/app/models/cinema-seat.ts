export class CinemaSeat {
    public id: string;
    public status: SeatStatus;

    constructor(id: string, status: SeatStatus) {
        if (!id || !status) {
            return null;
        }
        this.id = id;
        this.status = status;
    }

    public isFree(): boolean {
        return this.status === SeatStatus.FREE;
    }

    public isOccupied(): boolean {
        return this.status === SeatStatus.OCCUPIED;
    }

    public isSelected(): boolean {
        return this.status === SeatStatus.SELECTED;
    }

    public select(): void {
        this.status = SeatStatus.SELECTED;
    }

    public deselect(): void {
        this.status = SeatStatus.FREE;
    }

    public getId(): string {
        return this.id;
    }
}

export enum SeatStatus {
    FREE = 1,
    SELECTED,
    OCCUPIED,
}
