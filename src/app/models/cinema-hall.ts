import { CinemaSeat, SeatStatus } from './cinema-seat';

export class CinemaHall {
    public name: string;
    public rows: {
        rowNumber: number,
        seats: CinemaSeat[];
    }[];

    constructor(name: string, layout: CinemaHallLayout) {
        if (!name || !layout) {
            return null;
        }
        this.name = name;
        this.rows = [];
        Object.keys(layout.rows).forEach((rowN) => {
            const rowNumber = parseInt(rowN, 10);
            if (!rowNumber) { return null; }
            const seats = [];
            for (let i = 0; i < layout.rows[rowN].seatsCount; i++) {
                seats.push(new CinemaSeat([rowN, i + 1].join('-'), SeatStatus.FREE));
            }
            this.rows.push({ rowNumber, seats });
        });
    }

    public fillOccupiedSeats(occupiedSeatsLayout: OccupiedSeatsLayout): CinemaHall {
        if (!occupiedSeatsLayout || !this.name || !this.rows) { return null; }
        Object.keys(occupiedSeatsLayout).forEach((id) => {
            this.rows[occupiedSeatsLayout[id].row - 1]
                .seats[occupiedSeatsLayout[id].seat - 1]
                .status = SeatStatus.OCCUPIED;
        });
    }

    public getSeatsCount(): number {
        return this.rows.reduce((acc, el) => acc + el.seats.length, 0);
    }

    public getFreeSeatsCount(): number {
        return this.rows.reduce((acc, el) => acc + el.seats.filter((seat) => seat.isFree()).length, 0)
    }

    public getSelectedSeats(): CinemaSeat[] {
        const res = [];
        this.rows.forEach((el) => res.push(...el.seats.filter((seat) => seat.isSelected())));
        return res;
    }
}

export interface CinemaHallLayout {
    rows: {
        [rowNumber: number]: {
            seatsCount: number;
        };
    }
}

export interface OccupiedSeatsLayout {
    [id: string]: {
        row: number,
        seat: number
    }
}
