import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { environment } from '../environments/environment';

import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { MovieDetailsComponent } from './components/movie-details/movie-details.component';
import { MovieScheduleComponent } from './components/movie-schedule/movie-schedule.component';

const appRoutes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
    },
    { path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },

    // { path: 'login', component: LoginComponent },
    // { path: 'signup', component: SignUpComponent },
    // { path: 'profile', component: ProfileComponent },
    // { path: 'admin', component: AdminComponent },
    {
        path: 'movie/:id',
        component: MovieDetailsComponent,
        children: [
            // {path: 'schedule', component: MovieScheduleComponent},
        ]
    },
    { path: 'movie/:id/schedule', component: MovieScheduleComponent },
    // { path: 'schedule/:id/pay', component: MoviePayComponent },
    { path: '404', component: PageNotFoundComponent },
    { path: '**', redirectTo: '/404' }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(
            appRoutes,
            { enableTracing: !environment.production }
        ),
    ],
    exports: [
        RouterModule
    ],
    providers: [],
    declarations: []
})
export class AppRoutingModule { }
