import { TestBed, async } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './app.component';
import { NavBarModule } from './shared/nav-bar/nav-bar.module';
import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from './components/home/home.module';
import { MovieDetailsModule } from './components/movie-details/movie-details.module';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { MovieScheduleModule } from './components/movie-schedule/movie-schedule.module';


describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                PageNotFoundComponent
            ],
            imports: [
                NavBarModule,
                AppRoutingModule,
                HomeModule,
                MovieDetailsModule,
                MovieScheduleModule
            ],
            providers: [{provide: APP_BASE_HREF, useValue : '/' }]
        }).compileComponents();
    }));

    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
});
