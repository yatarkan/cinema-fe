import { Component, OnDestroy, OnInit } from '@angular/core';
import { MovieCardDetailed } from '../../../models/movie-card-detailed.model';
import { MovieDBService } from '../../../services/movie-db.service';
import { Subscription } from 'rxjs/Subscription';
import { MovieDBEndpoints } from '../../../utils/tmdb-api.types';

@Component({
    selector: 'app-home-discover',
    templateUrl: './home-discover.component.html',
    styleUrls: ['./home-discover.component.scss']
})
export class HomeDiscoverComponent implements OnInit, OnDestroy {

    public movies: MovieCardDetailed[];
    public discoverNavMenu: DiscoverNavItem[];
    private subscription: Subscription;

    constructor(private movieDBService: MovieDBService) {
        this.movies = [];
        this.discoverNavMenu = Object.keys(navItemsEndpointsMap).map((val) => {
            return {
                name: val,
                active: false
            } as DiscoverNavItem
        });
    }

    ngOnInit() {
        this.selectMoviesSorting(this.discoverNavMenu[0]);
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    public selectMoviesSorting(menuItem: DiscoverNavItem) {
        const i = this.discoverNavMenu.indexOf(menuItem);
        if (i < 0) { return null; }
        this.discoverNavMenu.forEach((el) => el.active = false);
        this.discoverNavMenu[i].active = true;
        this.subscription = this.movieDBService.getDetailedMovieCards(navItemsEndpointsMap[menuItem.name], 10)
            .subscribe(
                (res: MovieCardDetailed[]) => {
                    if (res) {
                        this.movies = res;
                    }
                },
                (err) => console.error(err)
            );
    }
}

interface DiscoverNavItem {
    name: string;
    active: boolean;
}

const navItemsEndpointsMap: { [key: string]: number; } = {
    'Popular': MovieDBEndpoints.POPULAR,
    'Top Rated': MovieDBEndpoints.TOP_RATED,
    'Upcoming': MovieDBEndpoints.UPCOMING,
};
