import { Component, Input, OnInit } from '@angular/core';
import { MovieCardDetailed } from '../../../../models/movie-card-detailed.model';

@Component({
    selector: 'app-movie-grid-item',
    templateUrl: './movie-grid-item.component.html',
    styleUrls: ['./movie-grid-item.component.scss']
})
export class MovieGridItemComponent implements OnInit {

    @Input()
    movie: MovieCardDetailed;

    constructor() { }

    ngOnInit() {
    }

}
