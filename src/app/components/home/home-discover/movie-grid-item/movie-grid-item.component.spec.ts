import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { MovieGridItemComponent } from './movie-grid-item.component';
import { NgxCarouselModule } from 'ngx-carousel';
import { HttpModule } from '@angular/http';
import { MovieCardDetailed } from '../../../../models/movie-card-detailed.model';
import { By } from '@angular/platform-browser';

describe('MovieGridItemComponent', () => {

    let comp:    MovieGridItemComponent;
    let fixture: ComponentFixture<MovieGridItemComponent>;
    let de:      DebugElement;
    let el:      HTMLElement;

    const mockInput: MovieCardDetailed = {
        id: 1,
        title: 'mock title',
        releaseDate: new Date(),
        rating: 8.5,
        posterUrl: 'http://via.placeholder.com/200x300',
        genres: ['genre1', 'genre2']
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ MovieGridItemComponent ],
            imports: [
                HttpModule,
                RouterTestingModule,
                NgxCarouselModule
            ],
            providers: [
            ]
        });

        fixture = TestBed.createComponent(MovieGridItemComponent);
        comp = fixture.componentInstance;
        comp.movie = mockInput;
        fixture.detectChanges();

    });

    it('should create component', async(() => {
        expect(comp).toBeTruthy();
    }));

    it('should create link to movie from input', async(() => {
        de = fixture.debugElement.query(By.css('a'));
        el = de.nativeElement;
        expect(el.getAttribute('href')).toEqual('/movie/' + mockInput.id);
    }));

    it('should create poster source from input', async(() => {
        de = fixture.debugElement.query(By.css('img'));
        el = de.nativeElement;
        expect(el.getAttribute('src')).toEqual(mockInput.posterUrl);
    }));

    it('should create movie rating from input', async(() => {
        de = fixture.debugElement.query(By.css('div.movie-rating-pill'));
        el = de.nativeElement;
        expect(el.textContent).toEqual(mockInput.rating.toString());
    }));

    it('should create date and genres details from input', async(() => {
        de = fixture.debugElement.query(By.css('h5.movie-details'));
        el = de.nativeElement;
        expect(el.textContent).toEqual(mockInput.releaseDate.getFullYear() + ' / ' + mockInput.genres.join(', '));
    }));

    it('should create movie title from input', async(() => {
        de = fixture.debugElement.query(By.css('h4.movie-title'));
        el = de.nativeElement;
        expect(el.textContent).toEqual(mockInput.title);
    }));
});
