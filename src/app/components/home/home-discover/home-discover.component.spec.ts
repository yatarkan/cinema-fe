import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeDiscoverComponent } from './home-discover.component';
import { MovieGridItemComponent } from './movie-grid-item/movie-grid-item.component';
import { NgxCarouselModule } from 'ngx-carousel';
import { HttpModule } from '@angular/http';
import { MovieDBService } from '../../../services/movie-db.service';
import { MovieCardDetailed } from '../../../models/movie-card-detailed.model';
import { By } from '@angular/platform-browser';

describe('HomeDiscoverComponent', () => {

    let comp:    HomeDiscoverComponent;
    let fixture: ComponentFixture<HomeDiscoverComponent>;
    let des:      DebugElement[];

    const mockMoviesArray: MovieCardDetailed[] = [{
            id: 1,
            title: 'mock title 1',
            releaseDate: new Date(),
            rating: 8.5,
            posterUrl: 'http://via.placeholder.com/200x300',
            genres: ['genre1', 'genre2']
        }, {
            id: 2,
            title: 'mock title 2',
            releaseDate: new Date(),
            rating: 7.7,
            posterUrl: 'http://via.placeholder.com/200x300',
            genres: ['genre3', 'genre4']
        }];

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                HomeDiscoverComponent,
                MovieGridItemComponent
            ],
            imports: [
                HttpModule,
                RouterTestingModule,
                NgxCarouselModule
            ],
            providers: [
                MovieDBService
            ]
        });

        fixture = TestBed.createComponent(HomeDiscoverComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges();

    });

    it('should create component', async(() => {
        expect(comp).toBeTruthy();
    }));

    it('should create empty movies array on init', async(() => {
        expect(comp.movies).toEqual([]);
    }));

    it('should select only first nav item on init', async(() => {
        des = fixture.debugElement.queryAll(By.css('a.navbar-link'));
        expect(des[0].nativeElement.className).toContain('navbar-link-active');
        des.forEach((link, i) => {
            if (i !== 0) {
                expect(des[i].nativeElement.className).not.toContain('navbar-link-active');
            }
        });
        expect(comp.discoverNavMenu[0].active).toBeTruthy();
        comp.discoverNavMenu.forEach((val, i) => {
            if (i !== 0) { expect(val.active).toBeFalsy(); }
        });
    }));

    it('should nav bar for discover section', async(() => {
        des = fixture.debugElement.queryAll(By.css('a.navbar-link'));
        const navItemsNames = comp.discoverNavMenu.map((val) => val.name);
        des.forEach((link, i) => {
            expect(link.nativeElement.textContent.trim()).toEqual(navItemsNames[i].trim());
        });
    }));

    it('should select nav item on click (add/remove active class, set selected boolean properties)', async(() => {
        des = fixture.debugElement.queryAll(By.css('a.navbar-link'));
        const selectedIndex = des.length - 1;
        des[selectedIndex].nativeElement.click();
        fixture.detectChanges();
        expect(des[selectedIndex].nativeElement.className).toContain('navbar-link-active');
        des.forEach((link, i) => {
            if (i !== selectedIndex) {
                expect(des[i].nativeElement.className).not.toContain('navbar-link-active');
            }
        });
        expect(comp.discoverNavMenu[selectedIndex].active).toBeTruthy();
        comp.discoverNavMenu.forEach((val, i) => {
            if (i !== selectedIndex) { expect(val.active).toBeFalsy(); }
        });
    }));

});
