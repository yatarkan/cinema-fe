import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxCarouselModule } from 'ngx-carousel';
import { HomeComponent } from './home.component';
import { HomeActualComponent } from './home-actual/home-actual.component';
import { HomeDiscoverComponent } from './home-discover/home-discover.component';
import { HomeNewsComponent } from './home-news/home-news.component';
import { HomeOffersComponent } from './home-offers/home-offers.component';
import { MovieDBService } from '../../services/movie-db.service';
import { MovieGridItemComponent } from './home-discover/movie-grid-item/movie-grid-item.component';
import { AppRoutingModule } from '../../app-routing.module';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        NgxCarouselModule,
        AppRoutingModule,
        RouterModule
    ],
    declarations: [
        HomeComponent,
        HomeActualComponent,
        HomeDiscoverComponent,
        HomeNewsComponent,
        HomeOffersComponent,
        MovieGridItemComponent,
    ],
    providers: [MovieDBService]
})
export class HomeModule { }
