import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeActualComponent } from './home-actual.component';
import { NgxCarouselModule } from 'ngx-carousel';
import { MovieDBService } from '../../../services/movie-db.service';
import { HttpModule } from '@angular/http';

describe('HomeActualComponent', () => {

    let comp:    HomeActualComponent;
    let fixture: ComponentFixture<HomeActualComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ HomeActualComponent ],
            imports: [
                HttpModule,
                RouterTestingModule,
                NgxCarouselModule
            ],
            providers: [
                MovieDBService,
            ]
        });

        fixture = TestBed.createComponent(HomeActualComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges();

    });

    it('should create component', async(() => {
        expect(comp).toBeTruthy();
    }));

    it('actualMovieCards should be an empty array by default', async(() => {
        expect(comp.actualMovieCards).toEqual([]);
    }));

    it('should have initial config for ngx-carousel with required parameter (grid)', async(() => {
        expect(comp.moviesCarousel).toBeTruthy();
        expect(comp.moviesCarousel).toEqual(jasmine.any(Object));
        expect(comp.moviesCarousel.grid as CarouselGridParam).toBeTruthy();
    }));

});

class CarouselGridParam {
    xs: number;
    sm: number;
    md: number;
    lg: number;
    all: number;
}
