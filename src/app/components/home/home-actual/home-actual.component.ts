import { Component, OnDestroy, OnInit } from '@angular/core';
import { MovieDBService } from '../../../services/movie-db.service';
import { Subscription } from 'rxjs/Rx';
import { NgxCarousel } from 'ngx-carousel';
import { MovieCard } from '../../../models/movie-card.model';

@Component({
    selector: 'app-home-actual',
    templateUrl: './home-actual.component.html',
    styleUrls: ['./home-actual.component.scss']
})
export class HomeActualComponent implements OnInit, OnDestroy {

    public moviesCarousel: NgxCarousel;

    public actualMovieCards: MovieCard[];
    private subscription: Subscription;

    constructor(private movieDbService: MovieDBService) {
        this.actualMovieCards = [];
    }

    ngOnInit() {
        this.moviesCarousel = {
            grid: {xs: 2, sm: 4, md: 5, lg: 6, all: 0},
            slide: 1,
            speed: 400,
            interval: 4000,
            point: {
                visible: true
            },
            touch: true,
            easing: 'ease-in',
            loop: true,
        };

        this.subscription =  this.movieDbService.getNowPlayingMovieCards(15)
            .subscribe(
                (res: MovieCard[]) => {
                    if (res) {
                        this.actualMovieCards = res;
                    }
                },
                (err) => console.error(err)
            );
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
