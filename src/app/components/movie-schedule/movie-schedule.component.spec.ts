import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { MovieScheduleComponent } from './movie-schedule.component';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { CinemaSeatComponent } from './cinema-seat/cinema-seat.component';
import { CinemaHallComponent } from './cinema-hall/cinema-hall.component';
import { FirebaseDbService } from '../../services/firebase-db.service';
import { SharingDataService } from '../../services/sharing-data.service';
import { PageNotFoundComponent } from '../../shared/page-not-found/page-not-found.component';

describe('MovieScheduleComponent', () => {

    let comp:    MovieScheduleComponent;
    let fixture: ComponentFixture<MovieScheduleComponent>;

    class MockRouter {
        navigateByUrl(url: string) { return url; }
    }

    const mockMovieTitle = 'Mock Movie Title';
    const mockMovieSchedule = {
        date: 'date',
        hall: 1,
        time: 'time',
        price: 100,
        occupiedSeats: {}
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MovieScheduleComponent,
                CinemaHallComponent,
                CinemaSeatComponent,
                PageNotFoundComponent
            ],
            imports: [
                CommonModule,
                HttpModule,
                FormsModule,
                AngularFireModule.initializeApp(environment.firebase),
                AngularFireDatabaseModule,
            ],
            providers: [
                SharingDataService,
                FirebaseDbService,
                { provide: Router, useClass: MockRouter },
                { provide: ActivatedRoute, useValue: {
                    snapshot: {
                        paramMap: {
                            get: (param: string) => param === 'id' ? 1 : null
                        }
                    },
                }
                }
            ]
        });
        fixture = TestBed.createComponent(MovieScheduleComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create component', async(() => {
        expect(comp).toBeTruthy();
    }));

    it('should display movie title', async(() => {
        comp.movieTitle = mockMovieTitle;
        fixture.detectChanges();
        const titleEl = fixture.debugElement.query(By.css('h1'));
        expect(titleEl.nativeElement.textContent.includes(mockMovieTitle)).toBeTruthy();
    }));

    it('should make all dropdowns disabled except date selection', async(() => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const dateSelect = fixture.debugElement.query(By.css('#showDate'));
            const hallSelect = fixture.debugElement.query(By.css('#showHall'));
            const timeSelect = fixture.debugElement.query(By.css('#showTime'));
            expect(dateSelect.nativeElement.disabled).toBeFalsy();
            expect(hallSelect.nativeElement.disabled).toBeTruthy();
            expect(timeSelect.nativeElement.disabled).toBeTruthy();
        });
    }));

    it('should enable hall selection when date selected', async(() => {
        comp.selectedDate = 'date';
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const dateSelect = fixture.debugElement.query(By.css('#showDate'));
            const hallSelect = fixture.debugElement.query(By.css('#showHall'));
            const timeSelect = fixture.debugElement.query(By.css('#showTime'));
            expect(dateSelect.nativeElement.disabled).toBeFalsy();
            expect(hallSelect.nativeElement.disabled).toBeFalsy();
            expect(timeSelect.nativeElement.disabled).toBeTruthy();
        });
    }));

    it('should enable time selection when hall selected', async(() => {
        comp.selectedDate = 'date';
        comp.selectedHallId = 1;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const dateSelect = fixture.debugElement.query(By.css('#showDate'));
            const hallSelect = fixture.debugElement.query(By.css('#showHall'));
            const timeSelect = fixture.debugElement.query(By.css('#showTime'));
            expect(dateSelect.nativeElement.disabled).toBeFalsy();
            expect(hallSelect.nativeElement.disabled).toBeFalsy();
            expect(timeSelect.nativeElement.disabled).toBeFalsy();
        });
    }));

    it('should enable cinema hall component when all inputs selected', async(() => {
        comp.selectedDate = 'date';
        comp.selectedHallId = 1;
        comp.selectedShow = mockMovieSchedule;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const hall = fixture.debugElement.query(By.css('app-cinema-hall'));
            expect(hall.attributes['ng-reflect-disabled']).toBe('false');
        });
    }));

    it('should create disabled button for buying tickets by default', async(() => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            const btn = fixture.debugElement.query(By.css('.btn-secondary'));
            expect(btn.nativeElement).toBeTruthy();
            expect(btn.nativeElement.disabled).toBeTruthy();
        });
    }));

});
