import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MovieScheduleComponent } from './movie-schedule.component';
import { CinemaHallComponent } from './cinema-hall/cinema-hall.component';
import { CinemaSeatComponent } from './cinema-seat/cinema-seat.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [MovieScheduleComponent, CinemaHallComponent, CinemaSeatComponent]
})
export class MovieScheduleModule { }
