import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieDetails } from '../../models/movie.model';
import { SharingDataService } from '../../services/sharing-data.service';
import { Subscription } from 'rxjs/Subscription';
import { CinemaHall } from '../../models/cinema-hall';
import { FirebaseDbService } from '../../services/firebase-db.service';
import { CinemaHallsResponse, MovieSchedule, cinemaHallPreview } from '../../utils/firebase-api.types';

@Component({
    selector: 'app-movie-schedule',
    templateUrl: './movie-schedule.component.html',
    styleUrls: [
        './movie-schedule.component.scss'
    ]
})
export class MovieScheduleComponent implements OnInit, OnDestroy {

    public movie: MovieDetails;
    public movieTitle: string;
    public movieShows: MovieSchedule[];
    public cinemaHalls: CinemaHallsResponse;

    public showsForDate: MovieSchedule[];
    public showsForDateAndHall: MovieSchedule[];
    public selectedShow: MovieSchedule;

    public selectedDate: string;
    public selectedHallId: number;
    public selectedHall: CinemaHall;

    private sharingDataServiceSub: Subscription;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private sanitizer: DomSanitizer,
        private sharingDataService: SharingDataService,
        private firebaseService: FirebaseDbService) {
            this.sharingDataServiceSub = this.sharingDataService.movieDetailsSource$.subscribe(
                (movieDetails) => {
                    this.movie = movieDetails;
                }
            );

            this.firebaseService.getScheduleByMovieId(this.route.snapshot.paramMap.get('id'))
                .first()
                .subscribe((schedule) => {
                    if (!schedule) {
                        console.error('Invalid schedule request');
                        this.router.navigateByUrl('/404');
                        return; }
                    this.movieTitle = schedule.title;
                    this.movieShows = Object.values(schedule.shows) as MovieSchedule[];
                });

            this.cinemaHalls = cinemaHallPreview;
            this.setDefaultHall();
            this.firebaseService.getCinemaHalls()
                .first()
                .subscribe((halls) => {
                    if (!halls) { return null; }
                    this.cinemaHalls = halls
                });
    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
        this.sharingDataServiceSub.unsubscribe();
    }

    public getAvailableDates(shows: MovieSchedule[]): string[] {
        if (!shows) { return null; }
        return Array.from(new Set(shows.map((el) => el.date)));
    }

    public getAvailableHallIds(shows: MovieSchedule[]): number[] {
        if (!shows) { return null; }
        return Array.from(new Set(shows.map((el) => el.hall)));
    }

    public getAvailableTime(shows: MovieSchedule[]): string[] {
        if (!shows) { return null; }
        return Array.from(new Set(shows.map((el) => el.time)));
    }

    public allOptionsSet(): boolean {
        return !!this.selectedDate && !!this.selectedHallId &&
                !!this.selectedHall && !!this.selectedShow;
    }

    public onChangeDate(date: string) {
        if (!date) { return null; }
        this.selectedShow = null;
        this.showsForDateAndHall = null;
        this.selectedHallId = null;
        this.setDefaultHall();
        this.showsForDate = this.movieShows.filter((el) => el.date === date);
    }

    public onChangeHall(hallId: number) {
        if (!hallId) { return null; }
        this.selectedShow = null;
        this.showsForDateAndHall = this.showsForDate.filter((el) => el.hall === +hallId);
        this.selectedHall = new CinemaHall(this.cinemaHalls[hallId].name, this.cinemaHalls[hallId].layout);
    }

    public onChangeTime(time: string) {
        if (!time) { return null; }
        [this.selectedShow] = this.showsForDateAndHall.filter((el) => el.time === time);
        this.selectedHall.fillOccupiedSeats(this.selectedShow.occupiedSeats);
    }

    public onBuyTickets() {
        console.log('Tickets to buy:');
        console.log(this.selectedHall.getSelectedSeats()
            .map((el) => 'Row: ' + el.id.split('-')[0] + ' Seat: ' + el.id.split('-')[1]));
    }

    public getBackdropStyle(imageUrl: string): SafeStyle {
        if (!imageUrl) { return null; }
        const style = `background-image: linear-gradient(rgba(14,24,55,0.8), rgba(14,24,55,0.8)),
            url(${imageUrl});`;
        return this.sanitizer.bypassSecurityTrustStyle(style);
    }

    private setDefaultHall() {
        const firstHall = Object.values(this.cinemaHalls)[0];
        this.selectedHall = new CinemaHall(firstHall.name, firstHall.layout);
    }
}

