import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { CinemaHallComponent } from './cinema-hall.component';
import { CinemaSeat, SeatStatus } from '../../../models/cinema-seat';
import { HttpModule } from '@angular/http';
import { By } from '@angular/platform-browser';
import { CinemaHall } from '../../../models/cinema-hall';
import { CinemaSeatComponent } from '../cinema-seat/cinema-seat.component';

describe('CinemaHallComponent', () => {

    let comp:    CinemaHallComponent;
    let fixture: ComponentFixture<CinemaHallComponent>;
    let de:      DebugElement;

    const mockHall = {
        name : 'Hall 1',
        layout: {
            rows : {
                '1' : {
                    seatsCount: 6
                },
                '2' : {
                    seatsCount: 8
                },
                '3' : {
                    seatsCount: 10
                },
                '4' : {
                    seatsCount: 10
                },
                '5' : {
                    seatsCount: 10
                },
                '6' : {
                    seatsCount: 10
                },
                '7' : {
                    seatsCount: 10
                }
            }
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ CinemaHallComponent, CinemaSeatComponent ],
            imports: [
                HttpModule,
            ],
            providers: []
        });

        fixture = TestBed.createComponent(CinemaHallComponent);
        comp = fixture.componentInstance;
        comp.cinemaHall = new CinemaHall(mockHall.name, mockHall.layout);
        comp.disabled = false;
        fixture.detectChanges();

    });

    it('should create component', async(() => {
        expect(comp).toBeTruthy();
    }));

    it('should create disabled cinema hall', async(() => {
        comp.disabled = true;
        fixture.detectChanges();
        const els = fixture.debugElement.queryAll(By.css('app-cinema-seat'));
        els.forEach((el) => {
            expect(el.nativeElement).toBeTruthy();
            expect(el.nativeElement.querySelector('input').disabled).toBeTruthy();
        });
    }));

    it('should make cinema hall enabled if input provided', async(() => {
        comp.disabled = false;
        fixture.detectChanges();
        const els = fixture.debugElement.queryAll(By.css('app-cinema-seat'));
        els.forEach((el) => {
            expect(el.nativeElement).toBeTruthy();
            expect(el.nativeElement.querySelector('input').disabled).toBeFalsy();
        });
    }));

    it('should create cinema hall according to layout', async(() => {
        const els = fixture.debugElement.queryAll(By.css('.cinema-hall-row'));
        expect(els.length).toBe(Object.keys(mockHall.layout.rows).length);
        els.forEach((el, i) => {
            const seats = el.queryAll(By.css('app-cinema-seat'));
            expect(seats.length).toBe(Object.values(mockHall.layout.rows)[i].seatsCount);
            seats.forEach((seat) => {
                expect(seat.nativeElement).toBeTruthy();
            });
        });
    }));

});
