import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CinemaHall } from '../../../models/cinema-hall';

@Component({
    selector: 'app-cinema-hall',
    templateUrl: './cinema-hall.component.html',
    styleUrls: ['./cinema-hall.component.scss']
})
export class CinemaHallComponent implements OnInit {

    @Input()
    public cinemaHall: CinemaHall;

    @Input()
    public disabled: boolean;

    constructor() {
    }

    ngOnInit() {
    }
}
