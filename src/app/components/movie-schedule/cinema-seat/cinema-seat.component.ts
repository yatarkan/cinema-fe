import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { CinemaSeat } from '../../../models/cinema-seat';

@Component({
    selector: 'app-cinema-seat',
    templateUrl: './cinema-seat.component.html',
    styleUrls: ['./cinema-seat.component.scss']
})
export class CinemaSeatComponent implements OnInit {

    @Input()
    public cinemaSeat: CinemaSeat;

    @Input()
    public disabled: boolean;

    @Output()
    public seatClick = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    public handleSeatClick($event) {
        if ($event.target.checked) {
            this.cinemaSeat.select();
            this.seatClick.emit($event);
            return;
        }
        this.cinemaSeat.deselect();
        this.seatClick.emit($event);
    }
}
