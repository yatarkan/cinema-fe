import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { CinemaSeatComponent } from './cinema-seat.component';
import { CinemaSeat, SeatStatus } from '../../../models/cinema-seat';
import { HttpModule } from '@angular/http';
import { By } from '@angular/platform-browser';

describe('CinemaSeatComponent', () => {

    let comp:    CinemaSeatComponent;
    let fixture: ComponentFixture<CinemaSeatComponent>;
    let de:      DebugElement;

    const mockSeatFree = new CinemaSeat('1-1', SeatStatus.FREE);
    const mockSeatSelected = new CinemaSeat('1-2', SeatStatus.SELECTED);
    const mockSeatOccupied = new CinemaSeat('1-3', SeatStatus.OCCUPIED);

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ CinemaSeatComponent ],
            imports: [
                HttpModule,
            ],
            providers: []
        });

        fixture = TestBed.createComponent(CinemaSeatComponent);
        comp = fixture.componentInstance;
        comp.cinemaSeat = mockSeatFree;
        comp.disabled = false;
        fixture.detectChanges();

    });

    it('should create component', async(() => {
        expect(comp).toBeTruthy();
    }));

    it('should create free seat', async(() => {
        comp.cinemaSeat = mockSeatFree;
        fixture.detectChanges();
        de = fixture.debugElement.query(By.css('input'));
        expect(de.nativeElement).toBeTruthy();
        expect(de.nativeElement.cheched).toBeFalsy();
    }));

    it('should create selected seat', async(() => {
        comp.cinemaSeat = mockSeatSelected;
        fixture.detectChanges();
        de = fixture.debugElement.query(By.css('input'));
        expect(de.nativeElement).toBeTruthy();
        expect(de.nativeElement.checked).toBeTruthy();
    }));

    it('should create occupied seat', async(() => {
        comp.cinemaSeat = mockSeatOccupied;
        fixture.detectChanges();
        de = fixture.debugElement.query(By.css('input'));
        expect(de.nativeElement).toBeTruthy();
        expect(de.nativeElement.disabled).toBeTruthy();
    }));

    it('should create disabled seat', async(() => {
        comp.cinemaSeat = mockSeatFree;
        comp.disabled = true;
        fixture.detectChanges();
        de = fixture.debugElement.query(By.css('input'));
        expect(de.nativeElement).toBeTruthy();
        expect(de.nativeElement.disabled).toBeTruthy();
    }));

    it('should select seat on click', async(() => {
        comp.cinemaSeat = mockSeatFree;
        fixture.detectChanges();
        de = fixture.debugElement.query(By.css('input'));
        expect(de.nativeElement).toBeTruthy();
        expect(de.nativeElement.checked).toBeFalsy();
        fixture.debugElement.nativeElement.querySelector('label').click();
        fixture.detectChanges();
        expect(de.nativeElement.checked).toBeTruthy();
    }));

    it('should deselect seat on click', async(() => {
        comp.cinemaSeat = mockSeatSelected;
        fixture.detectChanges();
        de = fixture.debugElement.query(By.css('input'));
        expect(de.nativeElement).toBeTruthy();
        expect(de.nativeElement.checked).toBeTruthy();
        fixture.debugElement.nativeElement.querySelector('label').click();
        fixture.detectChanges();
        expect(de.nativeElement.checked).toBeFalsy();
    }));
});
