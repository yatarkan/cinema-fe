import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Subscription } from 'rxjs/Subscription';
import { MovieDBService } from '../../services/movie-db.service';
import { MovieDetails } from '../../models/movie.model';
import { SharingDataService } from '../../services/sharing-data.service';
import { FirebaseDbService } from '../../services/firebase-db.service';

@Component({
    selector: 'app-movie-details',
    templateUrl: './movie-details.component.html',
    styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit, OnDestroy {

    public movie: MovieDetails;
    public scheduleAvailable: boolean;
    private routeSubscription: Subscription;
    private serviceSubscription: Subscription;

    constructor(private route: ActivatedRoute,
                private sanitizer: DomSanitizer,
                private movieDBService: MovieDBService,
                private sharingDataService: SharingDataService,
                private firebaseService: FirebaseDbService) {
        this.scheduleAvailable = false;
        this.firebaseService.isScheduleAvailable(this.route.snapshot.paramMap.get('id'))
            .first()
            .subscribe((el) => {
                this.scheduleAvailable = el;
            });
    }

    ngOnInit() {
        this.routeSubscription = this.route.params.subscribe((params) => {
            const movieId = +params['id'];
            this.serviceSubscription = this.movieDBService.getMovieDetailsWithCredits(movieId)
                .subscribe((res: MovieDetails) => {
                    if (!res) { return null; }
                    this.movie = res;
                    this.sharingDataService.saveMovieDetails(this.movie);
                });
        });
    }

    ngOnDestroy() {
        this.routeSubscription.unsubscribe();
        this.serviceSubscription.unsubscribe();
    }

    public getBackdropStyle(imageUrl: string): SafeStyle {
        if (!imageUrl) { return null; }
        const style = `background-image: linear-gradient(rgba(14,24,55,0.8), rgba(14,24,55,0.8)),
            url(${imageUrl});`;
        return this.sanitizer.bypassSecurityTrustStyle(style);
    }

}
