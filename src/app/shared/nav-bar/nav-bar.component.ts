import { Component, HostListener, OnInit } from '@angular/core';

@Component({
    selector: 'app-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

    public activeMenuItem: string;
    private mobileNav: boolean;

    constructor() {}

    ngOnInit() {
        this.mobileNav = this.isSmallScreen();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.mobileNav = this.isSmallScreen();
    }

    public isMobileNav(): boolean {
        return this.mobileNav;
    }

    public toggleNavBarMenu() {
        this.mobileNav = !this.mobileNav;
    }

    private isSmallScreen(): boolean {
        return window.innerWidth > 990;
    }

}
