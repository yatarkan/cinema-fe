import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from '@angular/http';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { NavBarModule } from './shared/nav-bar/nav-bar.module';
import { HomeModule } from './components/home/home.module';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';
import { MovieDetailsModule } from './components/movie-details/movie-details.module';
import { MovieScheduleModule } from './components/movie-schedule/movie-schedule.module';
import { SharingDataService } from './services/sharing-data.service';
import { FirebaseDbService } from './services/firebase-db.service';

@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,
        NavBarModule,
        HomeModule,
        MovieDetailsModule,
        MovieScheduleModule
    ],
    providers: [
        SharingDataService,
        FirebaseDbService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
