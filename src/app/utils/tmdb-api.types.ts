export enum MovieDBEndpoints {
    NOW_PLAYING = <any>'movie/now_playing',
    POPULAR = <any>'movie/popular',
    TOP_RATED = <any>'movie/top_rated',
    UPCOMING = <any>'movie/upcoming',
    GENRES = <any>'genre/movie/list',
    MOVIE_DETAILS = <any>'movie/'
}

export interface NowPlayingMoviesResponse extends PopularMoviesResponse {
    dates: object;
}

export interface PopularMoviesResponse {
    page: number;
    results: MovieListResultObject[];
    total_pages: number;
    total_results: number;
}

export interface GenresResponse {
    genres: Genre[];
}

export interface Genre {
    id: number;
    name: string;
}

export interface MovieListResultObject {
    poster_path: string | null,
    adult: boolean,
    overview: string,
    release_date: string,
    genre_ids: number[],
    id: number,
    original_title: string,
    original_language: string,
    title: string,
    backdrop_path: string | null,
    popularity: number,
    vote_count: number,
    video: boolean,
    vote_average: number
}

export function isMovieListResultObject(obj: any): obj is MovieListResultObject {
    const typedObj = <MovieListResultObject>obj;
    return  typedObj.poster_path !== undefined &&
            typedObj.adult !== undefined &&
            typedObj.overview !== undefined &&
            typedObj.release_date !== undefined &&
            typedObj.genre_ids !== undefined &&
            typedObj.id !== undefined &&
            typedObj.original_title !== undefined &&
            typedObj.original_language !== undefined &&
            typedObj.title !== undefined &&
            typedObj.backdrop_path !== undefined &&
            typedObj.popularity !== undefined &&
            typedObj.vote_count !== undefined &&
            typedObj.video !== undefined &&
            typedObj.vote_average !== undefined;
}

export class MovieDetailsWithCredits {
    id: number;
    title: string;
    overview: string;
    genres: Genre[];
    poster_path: string | null;
    backdrop_path: string | null;
    release_date: string;
    credits: {
        cast: CastPerson[],
        crew: CrewPerson[]
    };
    popularity: number;
    vote_count: number;
    vote_average: number;
}

interface CastPerson {
    cast_id: number;
    character: string;
    credit_id: string;
    gender: number;
    id: number;
    name: string;
    order: number;
    profile_path: string;
}

interface CrewPerson {
    credit_id: string;
    department: string;
    gender: number;
    id: number;
    job: string;
    name: string;
    profile_path: string;
}

export enum PosterSizes {
    WIDTH_92 = <any>'w92',
    WIDTH_154 = <any>'w154',
    WIDTH_185 = <any>'w185',
    WIDTH_342 = <any>'w342',
    WIDTH_500 = <any>'w500',
    WIDTH_780 = <any>'w780'
}
