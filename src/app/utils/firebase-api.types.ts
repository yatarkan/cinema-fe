import {CinemaHallLayout, OccupiedSeatsLayout} from '../models/cinema-hall';

export interface CinemaHallsResponse {
    [id: number]: {
        name: string;
        layout: CinemaHallLayout
    }
}

export interface MovieScheduleResponse {
    title: string;
    shows: {
        [id: number]: MovieSchedule;
    }
}

export interface MovieSchedule {
    date: string;
    hall: number;
    time: string;
    price: number;
    occupiedSeats: OccupiedSeatsLayout;
}

export const cinemaHallPreview = {
    '1' : {
        'name' : 'Hall 1',
        'layout': {
            'rows' : {
                '1' : {
                    'seatsCount': 6
                },
                '2' : {
                    'seatsCount': 8
                },
                '3' : {
                    'seatsCount': 10
                },
                '4' : {
                    'seatsCount': 10
                },
                '5' : {
                    'seatsCount': 10
                },
                '6' : {
                    'seatsCount': 10
                },
                '7' : {
                    'seatsCount': 10
                }
            }
        }
    }
};
