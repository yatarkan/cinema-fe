import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Rx';
import { MovieCard } from '../models/movie-card.model';
import { MovieCardDetailed } from '../models/movie-card-detailed.model';
import {
    MovieDBEndpoints, NowPlayingMoviesResponse, MovieDetailsWithCredits,
    GenresResponse, PopularMoviesResponse, isMovieListResultObject
} from '../utils/tmdb-api.types';
import { MovieDetails } from '../models/movie.model';

@Injectable()
export class MovieDBService {

    private baseUrl: string;

    constructor(private http: Http) {
        this.baseUrl = environment.theMovieDB.baseUrl;
    }

    public getMovieDetailsWithCredits(id: number): Observable<MovieDetails> {
        const queryUrl = this.baseUrl + MovieDBEndpoints.MOVIE_DETAILS + id +
            this.getApiKeyParam() + 'append_to_response=credits';
        return this.http.get(queryUrl)
            .first()
            .map((res: Response) => {
                const movieDetailsResponse = res.json() as MovieDetailsWithCredits;
                if (!movieDetailsResponse) { return null }
                return new MovieDetails(movieDetailsResponse);
            })
            .catch((error: any) => Observable.throw(error.json().error || 'TheMovieDB API error'));
    }

    public getDetailedMovieCards(source: MovieDBEndpoints, limit: number = 20): Observable<MovieCardDetailed[]> {
        const queryUrl = this.baseUrl + source + this.getApiKeyParam();
        return this.getGenres()
            .flatMap((genres: GenresResponse) => {
                return this.http.get(queryUrl)
                    .first()
                    .map((res: Response) => {
                        const moviesResponse = res.json();
                        return this.toMovieCardArrayWithGenres(moviesResponse, genres, limit);
                    })
                    .catch((error: any) => Observable.throw(error.json().error || 'TheMovieDB API error'));
            });
    }

    public getNowPlayingMovieCards(limit: number = 20): Observable<MovieCard[]> {
        const queryUrl = this.baseUrl + MovieDBEndpoints.NOW_PLAYING + this.getApiKeyParam();
        return this.http.get(queryUrl)
            .first()
            .map((res: Response) => {
                const moviesResponse = res.json() as NowPlayingMoviesResponse;
                if (!moviesResponse) { return null }
                return this.toMovieCardArray(moviesResponse, limit);
            })
            .catch((error: any) => Observable.throw(error.json().error || 'TheMovieDB API error'));
    }

    private getGenres(): Observable<GenresResponse> {
        const queryUrl = this.baseUrl + MovieDBEndpoints.GENRES + this.getApiKeyParam();
        return this.http.get(queryUrl)
            .first()
            .map((res: Response) => res.json() as GenresResponse)
            .catch((error: any) => Observable.throw(error.json().error || 'TheMovieDB API error'));
    }

    private toMovieCardArrayWithGenres(moviesResponse: PopularMoviesResponse,
                                       genresResponse: GenresResponse,
                                       limit: number = 20): MovieCardDetailed[] {
        if (!moviesResponse || !genresResponse.genres) { return null }
        let genres: string[];
        return moviesResponse.results
            .map((val) => {
                if (!isMovieListResultObject(val)) { return null; }
                genres = genresResponse.genres
                    .filter((genre) => val.genre_ids.includes(genre.id))
                    .map((filteredGenres) => filteredGenres.name);
                return new MovieCardDetailed(val, genres);
            })
            .slice(0, limit);
    }

    private toMovieCardArray(moviesResponse: NowPlayingMoviesResponse, limit: number = 20): MovieCard[] {
        if (!moviesResponse.results) { return null }
        if (!limit) {
            return moviesResponse.results.map((val) => new MovieCard(val));
        }
        return moviesResponse.results
            .map((val) => new MovieCard(val))
            .slice(0, limit);
    }

    private getApiKeyParam(): string {
        return '?api_key=' + environment.theMovieDB.apiKey + '&';
    }

}
