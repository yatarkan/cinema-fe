import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { MovieDetails } from '../models/movie.model';

@Injectable()
export class SharingDataService {

    private movieDetailsSource: Subject<MovieDetails> = new BehaviorSubject<MovieDetails>(null);
    movieDetailsSource$ = this.movieDetailsSource.asObservable();
    constructor() { }

    public saveMovieDetails(movieDetails: MovieDetails) {
        this.movieDetailsSource.next(movieDetails);
    }
}
