import { Injectable } from '@angular/core'
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { CinemaHallsResponse, MovieScheduleResponse } from '../utils/firebase-api.types';

@Injectable()
export class FirebaseDbService {

    constructor(private afdb: AngularFireDatabase) { }

    public getCinemaHalls(): Observable<CinemaHallsResponse> {
        return this.afdb.object('halls').valueChanges().first();
    }

    public getScheduleByMovieId(id: string): Observable<MovieScheduleResponse> {
        if (!id) { return Observable.of(null) }
        return this.afdb.object(`schedule/${id}`).valueChanges().first();
    }

    public isScheduleAvailable(id: string): Observable<boolean> {
        if (!id) { return Observable.of(null) }
        return this.afdb.object(`schedule/${id}`).valueChanges()
            .map((el) => !!el)
            .first();
    }
}
