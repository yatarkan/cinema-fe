export const environment = {
    production: true,
    firebase: {
        apiKey: 'AIzaSyAJ6Q6dWeusy0AOnT3JCVtAH3VgISZGvY4',
        authDomain: 'cinema-fe.firebaseapp.com',
        databaseURL: 'https://cinema-fe.firebaseio.com',
        projectId: 'cinema-fe',
        storageBucket: 'cinema-fe.appspot.com',
        messagingSenderId: '474576543822'
    },
    theMovieDB: {
        baseUrl: 'https://api.themoviedb.org/3/',
        imagesUrl: 'https://image.tmdb.org/t/p/',
        apiKey: '6085270c970ecf82ba2c149cc3dedcad'
    }
};
