// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyAJ6Q6dWeusy0AOnT3JCVtAH3VgISZGvY4',
        authDomain: 'cinema-fe.firebaseapp.com',
        databaseURL: 'https://cinema-fe.firebaseio.com',
        projectId: 'cinema-fe',
        storageBucket: 'cinema-fe.appspot.com',
        messagingSenderId: '474576543822'
    },
    theMovieDB: {
        baseUrl: 'https://api.themoviedb.org/3/',
        imagesUrl: 'https://image.tmdb.org/t/p/',
        apiKey: '6085270c970ecf82ba2c149cc3dedcad'
    }
};
