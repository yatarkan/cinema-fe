# Cinema-FE

Single-page application for cinema theatre with an option to select movie and buy the ticket online.

## Project structure

### Components

1. Home
    - Actual movies carousel (cards only)
    - Discover section (popular and recent movies)
    - News section
    - Special offers section
2. Actual movies section (now playing in cinema)
3. Movie information
4. Movie schedule
5. Movie ticket purchase
6. Auth
    - Register
    - Login
7. Account (?)
8. Admin (?)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
