import { CinemaFePage } from './app.po';

describe('cinema-fe App', () => {
  let page: CinemaFePage;

  beforeEach(() => {
    page = new CinemaFePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
